# MAAS JavaScript Utilities
A group of javascript helper functions for use across our Collection, Digital Labels and API.

This package starter kit is a fork of [www.kriasoft.com/babel-starter-kit](https://www.kriasoft.com/babel-starter-kit/).

## Getting Started
While developing, it is useful to run tests to check if functions are working properly.

```Bash
$ npm run test
$ npm run test:watch # Keep tests running while developing
```

### Update Build Config
If you created a new file, you'll need to add this to the list in `tools/build`.

```JS
const files = [
  'index',
  'elasticSearch',
  ...
  '[newFancyFilenameWithoutExtension]',
];
```

## Version Update Process
Make your usual code updates. Add new tests if needed. Run tests using `npm run test`.

### 1. Build
Before any changes are committed, make sure you run `npm run build`. This will create a compiled version in `/dist` and needs to be committed to the repo.

### 2. Commit
Git add and commit with appropriate comment.

### 3. Increment Version
Run `npm version patch|minor|major`. Semver should be followed (ie. fixup job = PATCH, new feature = MINOR, breaking change = MAJOR). This will increment version in `package.json` and also `git tag` and commit.

### 3. Push it
Usual `git push`. Make sure to push tags:

```Bash
$ git push --tags
```

## Testing
```Bash
$ npm run test # Run unit tests
$ npm run test:watch # Watch unit tests, good for test driven development.
```

## ENV
Make sure that you have these set up in your project's ENV file:

```
THUMBOR_SECRET=[SECRET]
THUMBOR_END_POINT=[URL]

CLOUDINARY_CLOUD=[ACCOUNT]
CLOUDINARY_KEY=[KEY]
CLOUDINARY_SECRET=[SECRET]
```

## List of Utilities

### getImageUrl
This function helps build image urls from our two cloud image servers - Thumbor and Cloudinary.

TODO: Add the rest in...

## Authors

* Kaho Cheung
* Lachlan Gordon
