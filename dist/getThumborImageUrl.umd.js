(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('dotenv'), require('detect-node'), require('thumbor')) :
	typeof define === 'function' && define.amd ? define(['dotenv', 'detect-node', 'thumbor'], factory) :
	(global['maas-js-utils'] = factory(global.dotenv,global.isNode,global.Thumbor));
}(this, (function (dotenv,isNode,Thumbor) { 'use strict';

dotenv = dotenv && 'default' in dotenv ? dotenv['default'] : dotenv;
isNode = isNode && 'default' in isNode ? isNode['default'] : isNode;
Thumbor = Thumbor && 'default' in Thumbor ? Thumbor['default'] : Thumbor;

function compact(items) {
  return items.filter(function (x) {

    return x !== null && x !== undefined;
  });
}

if (isNode) {
  dotenv.config();
}

function getThumborImageUrl(imageId, _ref) {
  var width = _ref.width,
      height = _ref.height,
      _ref$type = _ref.type,
      type = _ref$type === undefined ? 'thumbor' : _ref$type,
      _ref$smart = _ref.smart,
      smart = _ref$smart === undefined ? false : _ref$smart,
      _ref$filters = _ref.filters,
      filters = _ref$filters === undefined ? [] : _ref$filters;

  var result = void 0;

  var thumbor = new Thumbor(process.env.THUMBOR_SECRET, process.env.THUMBOR_END_POINT);

  if (imageId) {
    width = width === undefined ? 'orig' : width;
    height = height === undefined ? 'orig' : height;

    var thumborUrl = thumbor.setImagePath(imageId + '.jpg').resize(width, height).smartCrop(smart);

    result = Array.isArray(filters) && !filters.length ? thumborUrl.buildUrl() : thumborUrl.filter(compact(filters)).buildUrl();
  } else {
    result = null;
  }

  return result;
}

return getThumborImageUrl;

})));
//# sourceMappingURL=getThumborImageUrl.umd.js.map
