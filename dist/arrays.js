'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function compact(items) {
  return items.filter(function (x) {

    return x !== null && x !== undefined;
  });
}

exports.compact = compact;
//# sourceMappingURL=arrays.js.map
