function compact(items) {
  return items.filter(function (x) {

    return x !== null && x !== undefined;
  });
}

export { compact };
//# sourceMappingURL=arrays.es.js.map
