(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global['maas-js-utils'] = global['maas-js-utils'] || {})));
}(this, (function (exports) { 'use strict';

function compact(items) {
  return items.filter(function (x) {

    return x !== null && x !== undefined;
  });
}

exports.compact = compact;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=arrays.umd.js.map
