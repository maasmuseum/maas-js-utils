(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global['maas-js-utils'] = global['maas-js-utils'] || {})));
}(this, (function (exports) { 'use strict';

/*
 * TEXT UTILITIES
 * -------------------------------------------------------------------------- */

/*
 * Convert hidden linebreaks to html
 * -------------------------------------------------------------------------- */
function convertLineBreaks(text) {
  return text && text.replace(/(?:\r\n|\r|\n)/g, '<br />');
}

/*
 * Convert single quotes to smart quotes
 * http://leancrew.com/all-this/2010/11/smart-quotes-in-javascript/
 * -------------------------------------------------------------------------- */
function convertQuotes(text) {
  var a = null;

  if (text) {
    a = text;
    a = a.replace(/(^|[-\u2014\s(\["])'/g, "$1\u2018"); // opening singles
    a = a.replace(/'/g, "\u2019"); // closing singles & apostrophes
    a = a.replace(/(^|[-\u2014/\[(\u2018\s])"/g, "$1\u201C"); // opening doubles
    a = a.replace(/"/g, "\u201D"); // closing doubles
    a = a.replace(/--/g, "\u2014"); // em-dashes
  }

  return a;
}

/*
 * Trim string to maxChars if less than maxChars
 * -------------------------------------------------------------------------- */
function trim(string, maxChars) {
  var result = void 0;

  if (string && string.length > maxChars && typeof maxChars === 'number') {
    result = string.substring(0, maxChars) + '...';
  } else {
    result = string;
  }

  return result;
}

exports.convertLineBreaks = convertLineBreaks;
exports.convertQuotes = convertQuotes;
exports.trim = trim;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=text.umd.js.map
