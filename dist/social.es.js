/*
 * TEXT UTILITIES
 * -------------------------------------------------------------------------- */

/*
 * Convert hidden linebreaks to html
 * -------------------------------------------------------------------------- */


/*
 * Convert single quotes to smart quotes
 * http://leancrew.com/all-this/2010/11/smart-quotes-in-javascript/
 * -------------------------------------------------------------------------- */


/*
 * Trim string to maxChars if less than maxChars
 * -------------------------------------------------------------------------- */
function trim(string, maxChars) {
  var result = void 0;

  if (string && string.length > maxChars && typeof maxChars === 'number') {
    result = string.substring(0, maxChars) + '...';
  } else {
    result = string;
  }

  return result;
}

/*
 * SOCIAL MEDIA UTILITIES
 * -------------------------------------------------------------------------- */

// Build array of meta share properties for React Helmet
function buildShareMeta(_ref) {
  var fbAppId = _ref.fbAppId,
      title = _ref.title,
      description = _ref.description,
      imageUrl = _ref.imageUrl,
      url = _ref.url;


  var trimmedDescription = trim(description, 300);

  return [{ property: 'fb:app_id', content: fbAppId }, { property: 'og:title', content: title }, { property: 'og:description', content: trimmedDescription }, { property: 'og:image', content: imageUrl }, { property: 'og:url', content: url }, { property: 'twitter:card', content: 'summary_large_image' }, { property: 'twitter:site', content: '@maasmuseum' }, { property: 'twitter:title', content: title }, { property: 'twitter:description', content: trimmedDescription }, { property: 'twitter:image', content: imageUrl }];
}

export { buildShareMeta };
//# sourceMappingURL=social.es.js.map
