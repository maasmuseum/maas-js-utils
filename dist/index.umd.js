(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('dotenv'), require('detect-node'), require('cloudinary-core'), require('thumbor')) :
	typeof define === 'function' && define.amd ? define(['exports', 'dotenv', 'detect-node', 'cloudinary-core', 'thumbor'], factory) :
	(factory((global['maas-js-utils'] = global['maas-js-utils'] || {}),global.dotenv,global.isNode,global.cloudinary,global.Thumbor));
}(this, (function (exports,dotenv,isNode,cloudinary,Thumbor) { 'use strict';

dotenv = dotenv && 'default' in dotenv ? dotenv['default'] : dotenv;
isNode = isNode && 'default' in isNode ? isNode['default'] : isNode;
cloudinary = cloudinary && 'default' in cloudinary ? cloudinary['default'] : cloudinary;
Thumbor = Thumbor && 'default' in Thumbor ? Thumbor['default'] : Thumbor;

function compact(items) {
  return items.filter(function (x) {

    return x !== null && x !== undefined;
  });
}

if (isNode) {
  dotenv.config();
}

function getThumborImageUrl(imageId, _ref) {
  var width = _ref.width,
      height = _ref.height,
      _ref$type = _ref.type,
      type = _ref$type === undefined ? 'thumbor' : _ref$type,
      _ref$smart = _ref.smart,
      smart = _ref$smart === undefined ? false : _ref$smart,
      _ref$filters = _ref.filters,
      filters = _ref$filters === undefined ? [] : _ref$filters;

  var result = void 0;

  var thumbor = new Thumbor(process.env.THUMBOR_SECRET, process.env.THUMBOR_END_POINT);

  if (imageId) {
    width = width === undefined ? 'orig' : width;
    height = height === undefined ? 'orig' : height;

    var thumborUrl = thumbor.setImagePath(imageId + '.jpg').resize(width, height).smartCrop(smart);

    result = Array.isArray(filters) && !filters.length ? thumborUrl.buildUrl() : thumborUrl.filter(compact(filters)).buildUrl();
  } else {
    result = null;
  }

  return result;
}

if (isNode) {
  dotenv.config();
}

var cloudinaryConfig = {
  cloud_name: 'maasmuseum',
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET,
  secure: !!(process.env.URL && process.env.URL.indexOf('https') === 0)
};

var cl = cloudinary.Cloudinary.new(cloudinaryConfig);

function getImageUrl(imageId, _ref) {
  var width = _ref.width,
      height = _ref.height,
      _ref$type = _ref.type,
      type = _ref$type === undefined ? 'thumbor' : _ref$type,
      _ref$smart = _ref.smart,
      smart = _ref$smart === undefined ? false : _ref$smart;

  var result = void 0;

  if (imageId) {
    if (type === 'cloudinary') {
      result = cl.url(imageId, {
        width: width === 0 ? undefined : width,
        height: height === 0 ? undefined : height,
        crop: 'fill'
      });
    } else {
      width = width === undefined ? 'orig' : width;
      height = height === undefined ? 'orig' : height;

      result = getThumborImageUrl(imageId, {
        width: width,
        height: height,
        smart: smart
      });
    }
  } else {
    result = null;
  }

  return result;
}

/*
 * TEXT UTILITIES
 * -------------------------------------------------------------------------- */

/*
 * Convert hidden linebreaks to html
 * -------------------------------------------------------------------------- */
function convertLineBreaks(text) {
  return text && text.replace(/(?:\r\n|\r|\n)/g, '<br />');
}

/*
 * Convert single quotes to smart quotes
 * http://leancrew.com/all-this/2010/11/smart-quotes-in-javascript/
 * -------------------------------------------------------------------------- */
function convertQuotes(text) {
  var a = null;

  if (text) {
    a = text;
    a = a.replace(/(^|[-\u2014\s(\["])'/g, "$1\u2018"); // opening singles
    a = a.replace(/'/g, "\u2019"); // closing singles & apostrophes
    a = a.replace(/(^|[-\u2014/\[(\u2018\s])"/g, "$1\u201C"); // opening doubles
    a = a.replace(/"/g, "\u201D"); // closing doubles
    a = a.replace(/--/g, "\u2014"); // em-dashes
  }

  return a;
}

/*
 * Trim string to maxChars if less than maxChars
 * -------------------------------------------------------------------------- */

exports.getImageUrl = getImageUrl;
exports.getThumborImageUrl = getThumborImageUrl;
exports.convertLineBreaks = convertLineBreaks;
exports.convertQuotes = convertQuotes;
exports.compact = compact;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=index.umd.js.map
