import { expect } from 'chai';
import { convertLineBreaks, convertQuotes, trim } from '../src/text';

describe('Text', () => {

  describe('Line Breaks', () => {

    it('should replace line break with <br/> tag', () => {
      const result = convertLineBreaks('Text with line\nbreak');
      expect(result).to.be.equal('Text with line<br />break');
    });

  });

  describe('Quotes', () => {

    it('should replace singles quote with smart quotes', () => {
      const result = convertQuotes('\'smart\'');
      expect(result).to.be.equal('‘smart’');
    });

  })

  describe('Trim', () => {

    it('should reduce length of string and add ...', () => {
      const textToTrim = 'Normcore deep v pop-up ramps subway tile portland blog adaptogen gastropub craft beer farm-to-table wayfarers. Whatever occupy lyft, copper mug shoreditch organic vinyl bushwick retro adaptogen. Chambray retro next level roof party messenger bag raw denim taxidermy mustache celiac lyft whatever. Thundercats kitsch 8-bit tofu, prism venmo vice poutine man bun taiyaki mustache.';

      const result = trim(textToTrim, 300)
      expect(result).to.be.equal('Normcore deep v pop-up ramps subway tile portland blog adaptogen gastropub craft beer farm-to-table wayfarers. Whatever occupy lyft, copper mug shoreditch organic vinyl bushwick retro adaptogen. Chambray retro next level roof party messenger bag raw denim taxidermy mustache celiac lyft whatever. Thu...');
    });

  })

});
