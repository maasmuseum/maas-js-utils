import { expect } from 'chai';
import getImageUrl from '../src/getImageUrl';
import getThumborImageUrl from '../src/getThumborImageUrl';

const cloudinaryImageId = 'j0uje1fehtczwrigmrrm';
const thumborImageId = 506834;
const thumborICCImageId = 631838;

describe('Image', () => {

  describe('Cloudinary', () => {

    it('should return full size image url', () => {
      const url = getImageUrl(cloudinaryImageId, { type: 'cloudinary' })
      expect(url).to.be.equal('http://res.cloudinary.com/maasmuseum/image/upload/c_fill/j0uje1fehtczwrigmrrm');
    });

    it('should return 400 x 400 image', () => {
      const url = getImageUrl(cloudinaryImageId, { width: 400, height: 400, type: 'cloudinary' });
      expect(url).to.be.equal('http://res.cloudinary.com/maasmuseum/image/upload/c_fill,h_400,w_400/j0uje1fehtczwrigmrrm');
    });

    it('should return proportional 400px wide image', () => {
      const url = getImageUrl(cloudinaryImageId, { width: 400, type: 'cloudinary' });
      expect(url).to.be.equal('http://res.cloudinary.com/maasmuseum/image/upload/c_fill,w_400/j0uje1fehtczwrigmrrm');
    });

  });

  describe('Thumbor', () => {

    it('should return full size image url', () => {
      const url = getImageUrl(thumborImageId, {});
      expect(url).to.be.equal('https://d3ecqbn6etsqar.cloudfront.net/rhI4uG2_qjDsS0yz6mzHHLfcVBw=/origxorig/506834.jpg');
    });

    it('should return full size (origxorig) image url', () => {
      const url = getImageUrl(thumborImageId, { width: 'orig', height: 'orig' });
      expect(url).to.be.equal('https://d3ecqbn6etsqar.cloudfront.net/rhI4uG2_qjDsS0yz6mzHHLfcVBw=/origxorig/506834.jpg');
    });

    it('should return 400 x 400 image', () => {
      const url = getImageUrl(thumborImageId, { width: 400, height: 400 });
      expect(url).to.be.equal('https://d3ecqbn6etsqar.cloudfront.net/gRbBmjMZZGa_HEwaQOkbGAFcgTg=/400x400/506834.jpg');
    });

    it('should return proportional 400px wide image', () => {
      const url = getImageUrl(thumborImageId, { width: 400, height: 0 });
      expect(url).to.be.equal('https://d3ecqbn6etsqar.cloudfront.net/LeUy7XL1OeitGcX842INcJ8aGU0=/400x0/506834.jpg');
    });

    describe('Direct Function', () => {

      it('should return full size image url', () => {
        const url = getThumborImageUrl(thumborImageId, {});
        expect(url).to.be.equal('https://d3ecqbn6etsqar.cloudfront.net/rhI4uG2_qjDsS0yz6mzHHLfcVBw=/origxorig/506834.jpg');
      });

      it('should return 400 x 400 image', () => {
        const url = getThumborImageUrl(thumborImageId, { width: 400, height: 400 });
        expect(url).to.be.equal('https://d3ecqbn6etsqar.cloudfront.net/gRbBmjMZZGa_HEwaQOkbGAFcgTg=/400x400/506834.jpg');
      });

      it('should return proportional 400px wide image', () => {
        const url = getThumborImageUrl(thumborImageId, { width: 400, height: 0 });
        expect(url).to.be.equal('https://d3ecqbn6etsqar.cloudfront.net/LeUy7XL1OeitGcX842INcJ8aGU0=/400x0/506834.jpg');
      });

      it('should return  400 x 400 image with smart cropping without filters', () => {
        const url = getThumborImageUrl(thumborICCImageId, { width: 400, height: 400, smart: true, filters: [] });
        expect(url).to.be.equal('https://d3ecqbn6etsqar.cloudfront.net/r2qSLK9ik5BViVFKr2u-Z66qMW4=/400x400/smart/631838.jpg');
      });

      it('should return  400 x 400 image with smart cropping with strip_icc filter', () => {
        const url = getThumborImageUrl(thumborICCImageId, { width: 400, height: 400, smart: true, filters: ['strip_icc()'] });
        expect(url).to.be.equal('https://d3ecqbn6etsqar.cloudfront.net/MIOJfUMPTjhBYriQso3J_uoxM2I=/400x400/smart/filters:strip_icc()/631838.jpg');
      });

    });

  })

});
