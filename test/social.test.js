import { expect } from 'chai';
import { buildShareMeta } from '../src/social';

describe('Social', () => {

  describe('Build Share Meta', () => {

    it('should return share meta object for React Helmet', () => {

      const data = {
        fbAppId: '11111111111',
        title: 'My title',
        description: 'My description',
        imageUrl: 'https://maas.museum/images/something.jpg',
        url: 'https://maas.museum',
      };

      const result = buildShareMeta(data);
      expect(JSON.stringify(result)).to.be.equal(JSON.stringify([
        { property: 'fb:app_id', content: '11111111111' },
        { property: 'og:title', content: 'My title' },
        { property: 'og:description', content: 'My description' },
        { property: 'og:image', content: 'https://maas.museum/images/something.jpg' },
        { property: 'og:url', content: 'https://maas.museum' },
        { property: 'twitter:card', content: 'summary_large_image' },
        { property: 'twitter:site', content: '@maasmuseum' },
        { property: 'twitter:title', content: 'My title' },
        { property: 'twitter:description', content: 'My description' },
        { property: 'twitter:image', content: 'https://maas.museum/images/something.jpg' },
      ]));

    });

  });

});
