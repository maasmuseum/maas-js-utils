import { expect } from 'chai';
import { compact } from '../src/arrays';

// https://medium.com/building-ibotta/testing-arrays-and-objects-with-chai-js-4b372310fe6d
describe('Array', () => {

  describe('Test array utils function', () => {

    it('should return empty array after removing all the false values', () => {

      const test_array = [ null, undefined ];
      const result = compact(test_array);
      expect(result).to.eql([]);

    });

  });

});
