## MAAS JS Utils Change Log

All notable changes to this project will be documented in this file.

### [v1.1.0] - 2017-06-30

- Added `trim` and `buildShareMeta` functions and tests.

### [v1.0.6] - 2017-06-30

- Just updated this changelog.
