import { trim } from './text';

/*
 * SOCIAL MEDIA UTILITIES
 * -------------------------------------------------------------------------- */

// Build array of meta share properties for React Helmet
export function buildShareMeta({ fbAppId, title, description, imageUrl, url }) {

  const trimmedDescription = trim(description, 300);

  return [
    { property: 'fb:app_id', content: fbAppId },
    { property: 'og:title', content: title },
    { property: 'og:description', content: trimmedDescription },
    { property: 'og:image', content: imageUrl },
    { property: 'og:url', content: url },
    { property: 'twitter:card', content: 'summary_large_image' },
    { property: 'twitter:site', content: '@maasmuseum' },
    { property: 'twitter:title', content: title },
    { property: 'twitter:description', content: trimmedDescription },
    { property: 'twitter:image', content: imageUrl },
  ];

}
