/*
 * TEXT UTILITIES
 * -------------------------------------------------------------------------- */

/*
 * Convert hidden linebreaks to html
 * -------------------------------------------------------------------------- */
export function convertLineBreaks(text) {
  return text && text.replace(/(?:\r\n|\r|\n)/g, '<br />');
}

/*
 * Convert single quotes to smart quotes
 * http://leancrew.com/all-this/2010/11/smart-quotes-in-javascript/
 * -------------------------------------------------------------------------- */
export function convertQuotes(text) {
  let a = null;

  if (text) {
    a = text;
    a = a.replace(/(^|[-\u2014\s(\["])'/g, "$1\u2018");       // opening singles
    a = a.replace(/'/g, "\u2019");                            // closing singles & apostrophes
    a = a.replace(/(^|[-\u2014/\[(\u2018\s])"/g, "$1\u201c"); // opening doubles
    a = a.replace(/"/g, "\u201d");                            // closing doubles
    a = a.replace(/--/g, "\u2014");                           // em-dashes
  }

  return a;
};

/*
 * Trim string to maxChars if less than maxChars
 * -------------------------------------------------------------------------- */
export function trim(string, maxChars) {
  let result;

  if (string && string.length > maxChars && typeof maxChars === 'number') {
    result = string.substring(0, maxChars) + '...';
  } else {
    result = string;
  }

  return result;
}
