import dotenv from 'dotenv';
import isNode from 'detect-node';

if (isNode) {
  dotenv.config();
}

import Thumbor from 'thumbor';
import { compact } from './arrays';

export default function getThumborImageUrl(imageId, { width, height, type = 'thumbor', smart = false, filters = [] }) {
  let result;

  const thumbor = new Thumbor(process.env.THUMBOR_SECRET, process.env.THUMBOR_END_POINT);

  if (imageId) {
    width = (width === undefined) ? 'orig' : width;
    height = (height === undefined) ? 'orig' : height;

    let thumborUrl = thumbor.setImagePath(imageId + '.jpg').resize(width, height).smartCrop(smart);

    result = Array.isArray(filters) && !filters.length ?
                thumborUrl.buildUrl() : thumborUrl.filter( compact(filters) ).buildUrl();
  } else {
    result = null;
  }

  return result;
}
