import dotenv from 'dotenv';
import isNode from 'detect-node';

if (isNode) {
  dotenv.config();
}

import cloudinary from 'cloudinary-core';

import getThumborImageUrl from './getThumborImageUrl';

const cloudinaryConfig = {
  cloud_name: 'maasmuseum',
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET,
  secure: !!(process.env.URL && process.env.URL.indexOf('https') === 0),
};

const cl = cloudinary.Cloudinary.new(cloudinaryConfig);

export default function getImageUrl(imageId, { width, height, type = 'thumbor', smart = false }) {
  let result;

  if (imageId) {
    if (type === 'cloudinary') {
      result = cl.url(imageId, {
        width: width === 0 ? undefined : width,
        height: height === 0 ? undefined : height,
        crop: 'fill',
      });
    } else {
      width = (width === undefined) ? 'orig' : width;
      height = (height === undefined) ? 'orig' : height;

      result = getThumborImageUrl(imageId, {
        width,
        height,
        smart,
      });
    }
  } else {
    result = null;
  }

  return result;
}
