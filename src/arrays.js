export function compact(items) {
  return items.filter((x) => {

    return x !== null && x !== undefined;
  });
}